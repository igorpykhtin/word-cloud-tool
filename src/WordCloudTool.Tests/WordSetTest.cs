﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WordCloudTool.Tests
{
	[TestClass]
	public class WordSetTest
	{
		[TestMethod]
		public void TestWordSet_Word()
		{
			var wordSet = new WordSet();
			wordSet.Add("привет");

			Assert.IsTrue(wordSet.Contains("привет"));
			Assert.IsTrue(wordSet.Contains("ПРИВЕТ"));
			Assert.IsFalse(wordSet.Contains("слово"));

			wordSet.AddRange(new string[] { "небо", "земля" });

			Assert.IsTrue(wordSet.Contains("привет"));
			Assert.IsTrue(wordSet.Contains("небо"));
			Assert.IsTrue(wordSet.Contains("земля"));
			Assert.IsFalse(wordSet.Contains("слово"));
		}

		[TestMethod]
		public void TestWordSet_WordMaskedRight()
		{
			var wordSet = new WordSet();
			wordSet.Add("привет");
			wordSet.Add("котор*");

			Assert.IsTrue(wordSet.Contains("привет"));
			Assert.IsFalse(wordSet.Contains("слово"));
			Assert.IsTrue(wordSet.Contains("котор"));
			Assert.IsFalse(wordSet.Contains("кото"));
			Assert.IsTrue(wordSet.Contains("который"));
			Assert.IsTrue(wordSet.Contains("Которому"));

			wordSet.AddRange(new string[] { "что-*" });

			Assert.IsTrue(wordSet.Contains("привет"));
			Assert.IsTrue(wordSet.Contains("что-нибудь"));
			Assert.IsTrue(wordSet.Contains("что-то"));
			Assert.IsTrue(wordSet.Contains("ЧТО-ЛИБО"));
			Assert.IsFalse(wordSet.Contains("кое-что"));
			Assert.IsFalse(wordSet.Contains("небо"));
		}

		[TestMethod]
		public void TestWordSet_WordMaskedLeft()
		{
			var wordSet = new WordSet();
			wordSet.Add("привет");
			wordSet.Add("*-нибудь");

			Assert.IsTrue(wordSet.Contains("привет"));
			Assert.IsFalse(wordSet.Contains("слово"));
			Assert.IsTrue(wordSet.Contains("что-нибудь"));
			Assert.IsFalse(wordSet.Contains("что-то"));
			Assert.IsTrue(wordSet.Contains("КТО-НИБУДЬ"));
		}
	}
}
