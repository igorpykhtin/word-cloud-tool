﻿using System;
using System.ComponentModel;
using System.Text;
using CommandLine;
using CommandLine.Text;

namespace WordCloudTool
{
	class Options
	{
		private int _minWordLength = 1;

		[Option('s', "source", Required = true, HelpText = @"A source text file to be processed. Example: -s somefile.txt")]
		public string SourceFileName { get; set; }

		[Option('l', "limit", Required = false, HelpText = @"A number of words to output. Example: -l 100")]
		public int Limit { get; set; }

		[Option('b', "blacklist", Required = false, HelpText = @"A text file with blocked words. Example: -b blacklist.txt")]
		public string BlackListFileName { get; set; }

		[Option('e', "encoding", Required = false, DefaultValue = "utf-8", HelpText = "An encoding of the source file. Example: -e utf-8")]
		public string Encoding { get; set; } = "utf-8";

		[Option('m', "min-length", Required = false, DefaultValue = 1, HelpText = "A minimum length of a word. Example: -m 3")]
		[DefaultValue(1)]
		public int MinWordLength
		{
			get
			{
				return Math.Max(_minWordLength, 1);
			}
			set
			{
				_minWordLength = value;
			}
		}

		[Option('f', "format", Required = false, DefaultValue = OutputFormat.WF, HelpText = "A format to output a word ('W' - word only, 'WF' or 'FW' - word and frequency). Example: -f WF or -f W")]
		[DefaultValue(OutputFormat.W)]
		public OutputFormat Format { get; set; } = OutputFormat.W;

		[Option('p', "paging", Required = false, HelpText = "Makes a pause after filling the screen. Example: -p")]
		[DefaultValue(false)]
		public bool Paging { get; set; }

		[HelpOption]
		public string GetUsage()
		{
			return HelpText.AutoBuild(this, (HelpText current) => HelpText.DefaultParsingErrorsHandler(this, current));
		}

		public Encoding GetEncoding()
		{
			return Encoding != null ? System.Text.Encoding.GetEncoding(Encoding) : System.Text.Encoding.UTF8;
		}

		public enum OutputFormat
		{
			// Word
			W,
			// Word	Frequency
			WF,
			// Frequency Word
			FW,
		}
	}
}
