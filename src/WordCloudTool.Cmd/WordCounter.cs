﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WordCloudTool
{
	class WordCounter
	{
		private readonly IDictionary<string, WordCountPair> _words;

		public WordCounter(int capacity)
		{
			_words = new Dictionary<string, WordCountPair>(capacity, StringComparer.CurrentCulture);
		}

		public void AddWords(IEnumerable<string> words)
		{
			foreach (var word in words.Select(o => o.ToLower()))
			{
				WordCountPair pair;
				if (_words.TryGetValue(word, out pair))
				{
					pair.IncrementCount();
				}
				else
				{
					pair = new WordCountPair(word, 1);
					_words[word] = pair;
				}
			}
		}

		public IEnumerable<WordCountPair> GetWordStatistics(int limit)
		{
			var result = _words.Values.OrderByDescending(o => o.Count);
			return limit > 0 ? result.Take(limit) : result;
		}
	}

	class WordCountPair
	{
		public string Word { get; }
		public int Count { get; private set; }

		public WordCountPair(string word, int count)
		{
			Word = word;
			Count = count;
		}

		public void IncrementCount()
		{
			Count++;
		}
	}
}
