﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace WordCloudTool
{
	class WordReader
	{
		public static readonly Regex BlackListRegex = new Regex(@"[\w-\*]+");
		public static Regex GetWordRegex(int minWordLength) => new Regex($@"[\w]{{{minWordLength },}}(-[\w]+)?");

		private readonly Stream _stream;
		private readonly Encoding _encoding;
		private readonly Regex _wordRegex;

		public WordReader(Stream stream, Encoding encoding, int minWordLength)
			: this(stream, encoding, GetWordRegex(minWordLength))
		{
		}

		public WordReader(Stream stream, Encoding encoding, Regex regex)
		{
			_stream = stream;
			_encoding = encoding;
			_wordRegex = regex;
		}

		public IEnumerable<string> ReadWords()
		{
			var reader = new StreamReader(_stream, Encoding.UTF8);

			while (!reader.EndOfStream)
			{
				string line = reader.ReadLine();

				if (String.IsNullOrWhiteSpace(line))
				{
					continue;
				}

				var matches = _wordRegex.Matches(line);

				foreach (Match match in matches)
				{
					yield return match.Value;
				}
			}
		}
	}
}
