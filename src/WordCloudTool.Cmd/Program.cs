﻿using System;
using System.IO;
using System.Linq;

namespace WordCloudTool
{
	class Program
	{
		static void Main(string[] args)
		{
			var options = new Options();
			if (!CommandLine.Parser.Default.ParseArguments(args, options))
			{
				Environment.Exit(CommandLine.Parser.DefaultExitCodeFail);
			}

			try
			{
				Process(options);
			}
			catch (ArgumentException ex)
			{
				ConsoleEx.PrintError(ex, fullInfo: false);
				Environment.ExitCode = 1;
			}
			catch (NotSupportedException ex)
			{
				ConsoleEx.PrintError(ex, fullInfo: false);
				Environment.ExitCode = 1;
			}
			catch (Exception ex)
			{
				ConsoleEx.PrintError(ex, fullInfo: true);
				Environment.ExitCode = 1;
			}
		}

		private static void Process(Options options)
		{
			ConsoleEx.Paging = options.Paging;

			var blackList = new WordSet();

			if (options.BlackListFileName != null)
			{
				using (var fileStream = new FileStream(options.BlackListFileName, FileMode.OpenOrCreate, FileAccess.Read))
				{
					var reader = new WordReader(fileStream, options.GetEncoding(), WordReader.BlackListRegex);
					blackList.AddRange(reader.ReadWords());
				}
			}

			using (var fileStream = new FileStream(options.SourceFileName, FileMode.Open, FileAccess.Read))
			{
				var reader = new WordReader(fileStream, options.GetEncoding(), options.MinWordLength);

				var words = reader.ReadWords().Where(o => !blackList.Contains(o));

				var wordCounter = new WordCounter(4096);
				wordCounter.AddWords(words);

				ConsoleEx.Print(
					wordCounter.GetWordStatistics(options.Limit),
					pair => Print(pair, options.Format)
					);
            }
		}

		private static void Print(WordCountPair pair, Options.OutputFormat format)
		{
			string text;
			switch (format)
			{
				case Options.OutputFormat.FW:
					{
						text = $"{pair.Count}\t{pair.Word}";
						break;
					}
				case Options.OutputFormat.WF:
					{
						text = $"{pair.Word}\t{pair.Count}";
						break;
					}
				default:
					{
						text = pair.Word;
						break;
					}
			}
			Console.WriteLine(text);
		}
	}
}
