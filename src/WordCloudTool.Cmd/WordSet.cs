﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WordCloudTool
{
	class WordSet
	{
		private readonly ISet<string> _words = new HashSet<string>(StringComparer.CurrentCulture);
		private readonly IList<string> _maskedRight = new List<string>();
		private readonly IList<string> _maskedLeft = new List<string>();

		public IList<string> MaskedLeft
		{
			get
			{
				return _maskedLeft;
			}
		}

		public WordSet()
		{
		}

		public void AddRange(IEnumerable<string> words)
		{
			foreach (string word in words)
			{
				Add(word);
			}
		}

		public void Add(string word)
		{
			word = word.ToLower();

			if (word.EndsWith("*"))
			{
				_maskedRight.Add(word.TrimEnd('*'));
			}
			else if (word.StartsWith("*"))
			{
				_maskedLeft.Add(word.TrimStart('*'));
			}
			else
			{
				_words.Add(word);
			}
		}

		public bool Contains(string word)
		{
			word = word.ToLower();
			return _words.Contains(word.ToLower())
				|| _maskedRight.Any(part => word.StartsWith(part, StringComparison.CurrentCulture))
				|| _maskedLeft.Any(part => word.EndsWith(part, StringComparison.CurrentCulture));
		}
	}
}
