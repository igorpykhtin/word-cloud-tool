@echo off
rem change CHCP to UTF-8
chcp 65001
rem cls

set ToolPath=..\..\bin
set SourceFileName=d:\Downloads\DevChat\Output\DevChat_text.txt
set OutputFileName=output\all.txt
set BlackListFileName=blacklist.txt

mkdir output

del %OutputFileName%
%ToolPath%\WordCloudTool.Cmd.exe -s %SourceFileName% -b %BlackListFileName% -l 600 -m 3 -f fw >> %OutputFileName%
