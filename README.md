# Word Cloud Tool #

WordCloudTool is a command-line utility to analyze text files and export word statistics into a text file in a format, convenient for word cloud services like [http://www.wordclouds.com/](http://www.wordclouds.com/)

The latest binaries can be found on [Downloads](https://bitbucket.org/Cr7pto/word-cloud-tool/downloads) page.

### Command-line syntax ###

`WordCloudTool.Cmd.exe -s <sourcefile> [-l <limit>] [-b <blacklistfile>] [-e <encoding>] [-m <min-length>] [-f <W|WF|FW>] [-p]`


`-s`, `--source`	Required. A source text file to be processed.

`-l`, `--limit`	A number of words to output.

`-b`, `--blacklist`	A text file with blocked words.

`-e`, `--encoding`	An encoding of the source files (e.g. windows-1251 or utf-8). UTF-8 is used by default.

`-f`, `--format`	A format to output a word ('W' - word only, 'WF' or 'FW' - word and frequency).

`-m`, `--min-length`	A minimum length of a word.

`-p`, `--paging`	Makes a pause after filling the screen.

`--help`	Display the help screen.


### Examples ###

- Displays 20 most frequent words longer than 4 letters in the source UTF-8 text file (excluding the specified in blacklist.txt):

`WordCloudTool.Cmd.exe -s somesource.txt -b blacklist.txt -l 20 -m 4 -f FW -e utf-8`